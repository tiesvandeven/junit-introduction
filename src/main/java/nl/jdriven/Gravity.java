package nl.jdriven;

public class Gravity {
    private final String planet;
    private final double factor;

    public Gravity(String planet, double factor) {
        this.planet = planet;
        this.factor = factor;
    }

    public String getWeightForPlanet(double pounds) {
        return String.format("Earth weight of %s pounds is %s pounds on %s", pounds, pounds * factor, planet);
    }
}
